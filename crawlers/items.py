# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field
import re

class ProductItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = Field()
    url = Field()
    image = Field()
    description = Field()
    price = Field()
    brand = Field()
    model = Field()
    category = Field()

    def convert_from_xpath(self, selector, xpaths={}):
        for field, xpath in xpaths.iteritems():
            data = selector.xpath(xpath).extract()
            if isinstance(data, list):
                if len(data) > 0:
                    self[field] = data[0]
            else:
                self[field] = data


def convert_price(text_price):
    return re.search(ur"\$(\d+(.\d+){0,1})",text_price).group(1).replace(u',',u'')
