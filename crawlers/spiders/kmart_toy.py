import Cookie
import re
import urlparse
import scrapy
import crawlers.items


class KMartToySpider(scrapy.Spider):
    name = 'kmart_toy'
    start_urls = [
        'http://www.kmart.com.au/category/kids/toys-by-category/shop-all-toys/395006',
    ]
    load_more_params = {
        # 'beginIndex': '0',
        # 'catalogId': '10102',
        'contentBeginIndex': '0',
        'facet': '',
        'facetLimit': '',
        'isHistory': 'false',
        'langId': '-1',
        'maxPrice': '',
        'minPrice': '',
        'objectId': '',
        'orderBy': '5',
        'orderByContent': '',
        # 'pageSize': '30',
        'pageView': 'grid',
        'productBeginIndex': '0',
        'requesttype': 'ajax',
        'resultType': 'products',
        'searchTerm': '',
        # 'storeId': '10701'
    }

    item_url_xpath = '//div[contains(@class, "product_box")]/div/a[starts-with(@id, "catalogEntry_")]/@href'
    item_xpaths = {
        'image': '//img[@id="productMainImage"]/@src',
        'name': '//h1[starts-with(@id, "product_name")]/text()',
        'price': '//div[starts-with(@id, "price_display")]/span/text()',
        'brand': '//ul[@class="disc"]/li[contains(text(), "BRAND")]/span/text()',
        'model': '//span[starts-with(@id, "product_SKU")]/text()'
    }

    def parse(self, response):
        load_more_url = self.get_load_more_url(response)
        if load_more_url is None:
            return
        cookies = self.get_cookies(response)
        headers = {'Referer': response.url}
        num_items = self.get_num_products(response)
        self.load_more_params.update(self.get_id_params(load_more_url))
        begin_index = 0
        default_page_size = 30
        while begin_index < num_items:
            params = self.load_more_params.copy()
            page_size = min(default_page_size, num_items - begin_index)
            params['beginIndex'] = str(begin_index)
            params['pageSize'] = str(page_size)
            yield scrapy.FormRequest(
                load_more_url, method='POST', headers=headers, cookies=cookies,
                formdata=params, callback=self.parse_items)
            begin_index += page_size

    def parse_items(self, response):
        original_url = response.request.headers['Referer']
        urls = response.xpath(self.item_url_xpath).extract()
        for url in urls:
            yield scrapy.Request(urlparse.urljoin(original_url, url), callback=self.parse_item)
        self.logger.info('Parsed %s items.', len(urls))

    def parse_item(self, response):
        item = crawlers.items.ProductItem()
        item['url'] = response.url
        item['image'] = urlparse.urljoin(response.url, response.xpath(self.item_xpaths['image']).extract_first())
        item['name'] = response.xpath(self.item_xpaths['name']).extract_first()
        item['price'] = response.xpath(self.item_xpaths['price']).extract()[1]
        item['brand'] = response.xpath(self.item_xpaths['brand']).extract_first()
        item['model'] = response.xpath(self.item_xpaths['model']).extract_first()[5:]
        yield item

    def get_num_products(self, response):
        return int(response.xpath('//div[@class="product_no"]/text()').re_first('[0-9]+'))

    def get_load_more_url(self, response):
        pattern = re.compile("SearchBasedNavigationDisplayJS\.init\('([^']+)'\);")
        for selector in response.xpath('//script'):
            match = pattern.search(selector.extract())
            if match is not None:
                return match.group(1)
        return None

    def get_cookies(self, response):
        header = response.headers['Set-Cookie']
        cookie = Cookie.BaseCookie()
        if isinstance(header, basestring):
            cookie.load(header)
        else:
            for cookie_str in header:
                cookie.load(cookie_str)
        result = {}
        for k, v in cookie.iteritems():
            result[k] = v.value
        return result

    def get_id_params(self, load_more_url):
        url = urlparse.urlparse(load_more_url)
        params = urlparse.parse_qs(url.query)
        return {
            'catalogId': params['catalogId'][0],
            'storeId': params['storeId'][0]
        }
