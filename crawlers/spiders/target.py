import json

from scrapy.selector.unified import Selector
from scrapy.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
from crawlers.items import ProductItem, convert_price
import crawlers.categories


class TargetSpider(CrawlSpider):

    name = "Target"
    allowed_domains = ["www.target.com.au"]
    start_urls = (
        'http://www.target.com.au/c/toys/W138532',
    )
    category_mapping = crawlers.categories.mapping[name]

    exclude_paths = (
        '\/c\/women\/',
        '\/c\/baby\/',
        '\/c\/kids\/',
        '\/c\/men\/',
        '\/c\/home\/',
        '\/c\/body-beauty\/',
        '\/c\/lifestyle\/',
        '\/c\/christmas\/',
        '\/c\/school\/',
        '\/c\/clearance\/',
    )

    category_xpath = '//div[@id="megamenu"]//nav[@class="mm-Menu-nav"]/ul/li//h4'
    pager_xpath = '//div[contains(@class, "prod-refine-bottom")]/div[@class="pager-refine"]'
    product_xpath = '//li[contains(@class, "product")]'

    rules = (
        Rule(
            LinkExtractor(
                restrict_xpaths=(category_xpath, pager_xpath),
                deny=exclude_paths
            ),
            follow=True
        ),
        Rule(
            LinkExtractor(restrict_xpaths=(product_xpath, )),
            follow=False,
            callback='parse_product'
        ),
    )

    item_xpath = {
        'url': '//meta[@property="og:url"]/@content',
        'name': '//meta[@property="og:title"]/@content',
        'image': '//meta[@property="og:image"]/@content',
        'price': '//div[@class="prod-price-stock"]/div/span/text()',
        'model': '//div[@class="prod-detail"]/@data-base-prod-code',
    }

    def parse_product(self, response):
        selector = Selector(response)
        item = ProductItem()
        item.convert_from_xpath(selector, self.item_xpath)

        summary = selector.xpath('//div[@class="prod-detail"]/@data-ec-product').extract()[0]
        summary = json.loads(summary)
        item['brand'] = summary['brand']
        if self.category_mapping.has_key(summary['category']):
            item['category'] = self.category_mapping.get(summary['category'])
        else:
            return
        item['price'] = summary['price']

        yield item
