import re
import HTMLParser
import urlparse
from scrapy import Request, Spider
from crawlers.items import ProductItem


class MyerToySpider(Spider):
    name = 'myer_toy'
    start_urls = [
        'http://www.myer.com.au/shop/mystore/kids-toys/all-toys'
    ]

    page_xpath = '//a[starts-with(@id, "WC_CategoryOnlyResultsDisplay_links")]'
    item_count_xpath = '//span[@class="txt-item-count"]/text()'

    item_xpath = '//div[starts-with(@id, "baseContent_")]'
    url_xpath = 'div/a[starts-with(@id, "img")]/@href'
    image_xpath = 'div/a[starts-with(@id, "img")]/img/@src'
    price_xpath = './/strong/text()'
    price_now_xpath = './/span[@class="price-now"]/text()'
    name_xpath = './/a[span/@class="brand"]/text()'
    brand_xpath = './/a/span[@class="brand"]/text()'

    def parse(self, response):
        src = response.xpath(self.page_xpath).extract_first()
        link = self.extract_page_link(src)
        link = HTMLParser.HTMLParser().unescape(link)
        url = urlparse.urlparse(link)
        page_size = int(urlparse.parse_qs(url.query)['pageSize'][0])
        item_count = response.xpath(self.item_count_xpath).extract_first()
        item_count = int(item_count[0:item_count.find(' ')])
        begin_index = 0
        while begin_index < item_count:
            link = re.sub('beginIndex=[0-9]+', 'beginIndex=' + str(begin_index), link)
            begin_index += page_size
            yield Request(link, callback=self.parse_page)

    def extract_page_link(self, src):
        pattern = re.compile("loadContentURL\('([^']+)'\)")
        match = pattern.search(src)
        return match.group(1) if match is not None else None

    def parse_page(self, response):
        page_url = response.url
        for item_selector in response.xpath(self.item_xpath):
            item = ProductItem()
            item['url'] = urlparse.urljoin(page_url, item_selector.xpath(self.url_xpath).extract_first())
            item['image'] = urlparse.urljoin(page_url, item_selector.xpath(self.image_xpath).extract_first())
            item['price'] = self.get_price(item_selector)
            item['name'] = ''.join(item_selector.xpath(self.name_xpath).extract()).strip()
            item['brand'] = item_selector.xpath(self.brand_xpath).extract_first()
            pos = item['url'].rfind('-') + 1
            item['model'] = item['url'][pos:]
            yield item

    def get_price(self, item_selector):
        price = item_selector.xpath(self.price_xpath).extract()
        if len(price) > 0:
            return price[0]
        price = item_selector.xpath(self.price_now_xpath).extract()
        if len(price) > 0:
            pos = price[0].find('$')
            return price[0][pos:]
        return None
