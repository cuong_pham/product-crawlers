import urlparse
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from crawlers.items import ProductItem


def extract_product_link(value):
    url = urlparse.urlparse(value)
    if url.hostname.endswith('bigw.com.au'):
        return value
    params = urlparse.parse_qs(url.query)
    if 'p' in params:
        return 'https://www.bigw.com.au/product/p/' + params['p'][0]
    return None


class BigWToySpider(CrawlSpider):
    name = 'bigw_toy'
    start_urls = [
        'https://www.bigw.com.au/toys/c/68/'
    ]

    category_xpath = '//li[contains(@class, "list-group-item")]/span/span'
    pager_xpath = '//ul[contains(@class, "pagination")]/li[contains(@class, "next")]'
    product_xpath = '//span[@class="details"]/h5/strong'

    item_xpaths = {
        'image': '//img[contains(@class, "img-thumbnail")]/@src',
        'name': '//h1[@itemprop="name"]/text()',
        'price': '//span[@itemprop="price" and not(@id)]/text()',
        'brand': '//span[@itemprop="brand"]/text()',
        'model': ('//ul[contains(@class, "specifications")]'
                  '/li[div/strong/text()="Reference Number"]'
                  '/div[contains(@class, "col-lg-7")]/text()'
                  )
    }

    rules = (
        Rule(LinkExtractor(restrict_xpaths=(category_xpath, pager_xpath)), follow=True),
        Rule(LinkExtractor(restrict_xpaths=product_xpath, process_value=extract_product_link), follow=False,
             callback='parse_product')
    )

    def parse_product(self, response):
        item = ProductItem()
        item['url'] = response.url
        item['image'] = urlparse.urljoin(response.url, response.xpath(self.item_xpaths['image']).extract_first())
        item['name'] = response.xpath(self.item_xpaths['name']).extract_first()
        item['price'] = response.xpath(self.item_xpaths['price']).extract_first().strip()
        item['brand'] = response.xpath(self.item_xpaths['brand']).extract_first()
        item['model'] = response.xpath(self.item_xpaths['model']).extract_first()
        yield item
