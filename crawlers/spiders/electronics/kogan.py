# -*- coding: utf-8 -*-
import scrapy
import json
from crawlers.items import ProductItem
import urllib, urlparse


class KoganSpider(scrapy.Spider):
    name = "Kogan"
    allowed_domains = ["kogan.com"]
    departments = [
         'televisions',
         'phones',
         'tablets-laptops',
         'cameras',
         'audio',
         'home-appliances'
    ]
    start_urls = [
        'https://www.kogan.com/api/v1/products/?store=au&department={}&page=1'.format(department)
        for department in departments
    ]
    item_mapping = {
        'price': 'your_price',
        'image': 'image',
        'url': 'url',
        'brand': 'brand',
        'name': 'title',
        'model': 'sku'
    }
    def parse(self, response):

        json_response = json.loads(response.body_as_unicode())
        current_page = json_response['meta']['page']
        has_next = json_response['meta']['has_next']

        objects = json_response.get('objects',[])
        for kItem in objects:
            item = ProductItem()
            for key, value in self.item_mapping.iteritems():
                item[key] = kItem[value]
            item['url'] = 'https://www.kogan.com/{}'.format(item['url'])
            yield item

        if has_next:
            params = {'page': current_page+1}
            url_parts = list(urlparse.urlparse(response.url))
            query = dict(urlparse.parse_qsl(url_parts[4]))
            query.update(params)

            url_parts[4] = urllib.urlencode(query)

            request_url = urlparse.urlunparse(url_parts)

            yield scrapy.Request(request_url, callback=self.parse)
