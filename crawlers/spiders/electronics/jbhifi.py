# -*- coding: utf-8 -*-
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider, Rule
from crawlers.items import ProductItem
from scrapy.linkextractors import LinkExtractor

class JbhifiSpider(CrawlSpider):

    name = "JB Hifi"
    allowed_domains = ["www.jbhifi.com.au"]
    start_urls = (
        'https://www.jbhifi.com.au/',
    )
    excluded_paths = (
        "jbhifi\.com\.au\/music\/",
        "jbhifi\.com\.au\/movies-tv-shows\/",
        "jbhifi\.com\.au\/musical-instruments\/",

    )

    navlinks_xpath = '//div[@id="products_menu"]/div/ul[@class="products"]'
    product_xpath = '//div[@id="productsContainer"]/ul[contains(@class,"grid")]/li[contains(@class,"load-more-results")]/div[contains(@class,"product-tile")]'
    pager_xpath = '//span[@class="pagNum"]'

    rules = (
        Rule(
            LinkExtractor(
                restrict_xpaths=(navlinks_xpath, pager_xpath),
                deny=excluded_paths,
            ),
            follow=True
        ),
        Rule(
            LinkExtractor(
                restrict_xpaths=(product_xpath,),
                deny=excluded_paths,
            ),
            follow=False,
            callback='parse_product'
        ),
    )

    item_xpath = {
        'name': '//meta[@property="name"]/@content',
        'price': '//meta[@property="price"]/@content',
        'brand': '//meta[@property="brand"]/@content',
        'model': '//meta[@property="model"]/@content',
        'image': '//meta[@property="image"]/@content',
    }

    def parse_product(self, response):
        selector = Selector(response)
        item = ProductItem()
        item['url'] = response.url
        item.convert_from_xpath(selector, self.item_xpath)
        item['image'] = "//www.jbhifi.com.au/{}".format(item['image'])
        yield item





