# -*- coding: utf-8 -*-
from crawlers.items import ProductItem, convert_price
from scrapy.selector import Selector
from scrapy.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
import re
class DicksmithsSpider(CrawlSpider):
    name = "Dicksmith"
    allowed_domains = ["dicksmith.com.au"]

    start_urls = (
        'https://www.dicksmith.com.au/',
    )

    navlinks_xpath = '//ul[@id="nav"]'
    product_xpath = '//ul[contains(@class,"products-grid")]//li[@class="item"]|//ol[@id="products-list"]'
    pager_xpath = '//div[contains(@class,"pager")]//div[contains(@class,"pages")]'
    rules = (
        Rule(
            LinkExtractor(
                restrict_xpaths=(navlinks_xpath, pager_xpath),
            ),
            follow=True
        ),
        Rule(
            LinkExtractor(
                restrict_xpaths=(product_xpath,),
            ),
            follow=False,
            callback='parse_product'
        ),
    )
    item_xpath = {
        'name': '//div[@class="breadcrumbs"]//li[@class="product"]/strong/text()',
        'image': '//img[@id="Zoom-Box"]/@src',
        'brand': '//span[@id="tagmgr"]/script/text()',
        'model': '//div[@class="flt-l"]/span[@class="product-code"]/text()',
        'price': '//div[@class="price-box"]//span[@class="price"]/text()'
    }

    def parse_product(self, response):
        selector = Selector(response)
        item = ProductItem()
        item['url'] = response.url
        item.convert_from_xpath(selector, self.item_xpath)
        item['price'] = convert_price(item['price'])
        try:
            brand = re.search(ur"'brand'\s*:\s*'(.*?)'", item['brand'])
            if brand is not None:
                item['brand'] = brand.group(1)
            else:
                item['brand'] = u''
        except IndexError:
            pass
        yield item
