from scrapy.selector.unified import Selector
from scrapy.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
from crawlers.items import ProductItem, convert_price


class TheGoodGuysSpider(CrawlSpider):

    name = "The Good Guys"
    allowed_domains = ["www.thegoodguys.com.au"]
    start_urls = (
        'http://www.thegoodguys.com.au/sitemap',
    )

    category_xpath = '//li[@class="h3" and contains(@id, "SiteMap_")]'
    pager_xpath = '//div[@id="pagination_header_down"]//div[contains(@class,"pageControl")]'
    product_xpath = '//div[@class="product_info"]'

    rules = (
        Rule(
            LinkExtractor(restrict_xpaths=(category_xpath, pager_xpath)),
            follow=True
        ),
        Rule(
            LinkExtractor(restrict_xpaths=(product_xpath, )),
            follow=False,
            callback='parse_product'
        ),
    )

    item_xpath = {
        'url': '//meta[@property="og:url"]/@content',
        'name': '//meta[@property="og:title"]/@content',
        'price': '//div[contains(@class,"prod_form")]//input[@name="productPrice"]/@value',
        'image': '//meta[@property="og:image"]/@content',
        'model': '//div[contains(@class,"prod_form")]//input[@name="manufacturerNumber"]/@value',
        'brand': '//div[contains(@class,"prod_form")]//input[@id="manufacturerName"]/@value',
    }

    def parse_product(self, response):
        selector = Selector(response)
        item = ProductItem()
        item.convert_from_xpath(selector, self.item_xpath)
        yield item
