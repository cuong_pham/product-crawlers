# -*- coding: utf-8 -*-
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider, Rule, Spider
from crawlers.items import ProductItem, convert_price
from scrapy.linkextractors import LinkExtractor
import re

class HarveyNormanSpider(CrawlSpider):
    name = "Harvey Norman"
    allowed_domains = ["harveynorman.com.au"]
    start_urls = (
        'http://www.harveynorman.com.au/sitemap',
    )
    exclude_paths = (
        'http:\/\/www.harveynorman.com.au\/big-buys\/',
        'http:\/\/www.harveynorman.com.au\/sports-camping-pets\/',
        'http:\/\/www.harveynorman.com.au\/toys-kids-baby\/',
        'http:\/\/www.harveynorman.com.au\/carpet-flooring-rugs\/',
        'http:\/\/www.harveynorman.com.au\/bathroom-tiles-renovations\/',
        'http:\/\/www.harveynorman.com.au\/beds-manchester\/',
        'http:\/\/www.harveynorman.com.au\/furniture-outdoor-bbqs\/',
        'http:\/\/www.harveynorman.com.au\/hair-body-care\/',
    )

    navlinks_xpath = '//div[@id="content"]//ul[not(contains(@class,"addto"))]'
    pager_xpath = '//div[contains(@class,"pager")]'
    product_xpath = '//div[@id="category-grid"]//div[contains(@class,"row")]//div[contains(@class,"photo-box")]'

    rules = (
        Rule(
            LinkExtractor(
                restrict_xpaths=(navlinks_xpath, pager_xpath, ),
                deny=exclude_paths,
            ),
            follow=True
        ),
        Rule(
            LinkExtractor(
                restrict_xpaths=(product_xpath,),
            ),
            follow=False,
            callback='parse_item'
        ),
    )

    item_xpath = {
        'name': '//meta[@property="og:title"]/@content',
        'price': '//div[@id="product-view-price"]/@data-price',
        'image': '//meta[@property="og:image"]/@content',
    }

    def parse_item(self, response):
        selector = Selector(response)
        item = ProductItem()
        item.convert_from_xpath(selector, self.item_xpath)
        item['url'] = response.url
        script_texts = selector.xpath('//script').extract()
        try:
            for script in script_texts:
                matched_text = re.search(ur"PAGE_INFO\s*=\s*\{(.*?)\};", script)
                if matched_text is not None:
                    data = matched_text.group(1)
                    item['brand'] = re.search(ur'\"brand\"\s*:\s*\"(.*?)\"', data).group(1)
                    item['model'] = re.search(ur'\"sku\"\s*:\s*\"(.*?)\"', data).group(1)
                    yield item
                    break
                    
        except IndexError:
            pass
