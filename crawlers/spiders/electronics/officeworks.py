# -*- coding: utf-8 -*-
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider, Rule
from crawlers.items import ProductItem, convert_price
from scrapy.linkextractors import LinkExtractor


class OfficeworksSpider(CrawlSpider):
    name = "Officeworks"
    allowed_domains = ["officeworks.com.au"]
    start_urls = (
        'http://www.officeworks.com.au/shop/officeworks/c/technology/',
    )

    include_paths = (
        '\/shop\/officeworks\/c\/technology\/',
    )

    navlinks_xpath = '//div[@id="sub-category-5"]//ul'
    subcategories_xpath = '//div[contains(@class,"subcategories")]//div[contains(@class,"subcat")]'
    pager_xpath = '//div[@id="content"]/div[contains(@class,"seo-pagination-urls")]'
    product_xpath = '//h2[contains(@class,"product_name")]'

    rules = (
        Rule(
            LinkExtractor(
                restrict_xpaths=(navlinks_xpath, subcategories_xpath, pager_xpath, ),
                allow=include_paths
            ),
            follow=True
        ),
        Rule(
            LinkExtractor(
                restrict_xpaths=(product_xpath,),
            ),
            follow=False,
            callback='parse_item'
        ),
    )

    item_xpath = {
        'name': '//meta[@itemprop="name"]/@content',
        'price': '//meta[@itemprop="price" or @itemprop="highPrice"]/@content',
        'brand': '//meta[@itemprop="brand"]/@content',
        'model': '//meta[@itemprop="sku"]/@content',
        'image': '//meta[@itemprop="image"]/@content',
    }

    def parse_item(self, response):
        selector = Selector(response)
        item = ProductItem()
        item['url'] = response.url
        item.convert_from_xpath(selector, self.item_xpath)
        item['price'] = convert_price(item['price'])
        yield item
